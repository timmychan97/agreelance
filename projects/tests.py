import os
import random
import string
from unittest import skip

from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client
from django.urls import reverse

import projects
from notification.models import Notification
from .forms import TaskOfferForm
from .models import Project, ProjectCategory, Task, TaskOffer, Team, TaskFile, TaskFileTeam, Delivery
from .views import get_user_task_permissions


class TakeOfferFormBoundaryTests(TestCase):
    """
    Assumptions:
     - No dependencies between the input parameters. N-way testing are not implemented.
     - No testing for string variables, as it is within scope of boundary testing. Except for length of string
    """

    def setUp(self):
        self.title = {
            'valid': [
                'testTitle',
                't' * 200,
            ],
            'invalid': [
                '',
                't' * 201,
            ]
        }
        self.description = {
            'valid': [
                'testDescription',
                't' * 500,
            ],
            'invalid': [
                '',
                't' * 501,
            ]
        }
        self.price = {
            'valid': [
                100,
                0,
            ],
            'invalid': [
                # -1,  # The form do not properly define the boundaries.
            ]
        }

    def test_title(self):
        for valid in self.title['valid']:
            form = TaskOfferForm(
                {'title': valid, 'description': self.description['valid'][0], 'price': self.price['valid'][0]})
            self.assertTrue(form.is_valid())
        for invalid in self.title['invalid']:
            form = TaskOfferForm(
                {'title': invalid, 'description': self.description['valid'][0], 'price': self.price['valid'][0]})
            self.assertFalse(form.is_valid())

    def test_description(self):
        for valid in self.description['valid']:
            form = TaskOfferForm(
                {'title': self.title['valid'][0], 'description': valid, 'price': self.price['valid'][0]})
            self.assertTrue(form.is_valid())
        for invalid in self.description['invalid']:
            form = TaskOfferForm(
                {'title': self.title['valid'][0], 'description': invalid, 'price': self.price['valid'][0]})
            self.assertFalse(form.is_valid())

    def test_price(self):
        for valid in self.price['valid']:
            form = TaskOfferForm(
                {'title': self.title['valid'][0], 'description': self.description['valid'][0], 'price': valid})
            self.assertTrue(form.is_valid())
        for invalid in self.price['invalid']:
            form = TaskOfferForm(
                {'title': self.title['valid'][0], 'description': self.description['valid'][0], 'price': invalid})
            self.assertFalse(form.is_valid())


class ProjectsWithFilterTests(TestCase):
    fixtures = ['test_seed_standard.json']

    def setUp(self):
        self.client = Client()
        self.url = reverse(projects.views.projects_with_filter)

    def test_search(self):
        categories = ['Painting', 'Cleaning', 'Gardening']
        search = 'Fix'
        response_html = self.request_html(search=search, categories=categories, sort_alpha=False, _reversed=False)

        self.assert_projects_in_response_ordered(response_html, [1, 2, 5])

    def test_search_sort_alpha(self):
        categories = ['Painting', 'Cleaning', 'Gardening']
        search = 'Fix'
        response_html = self.request_html(search=search, categories=categories, sort_alpha=True, _reversed=False)

        self.assert_projects_in_response_ordered(response_html, [5, 2, 1])

    def test_search_reverse(self):
        categories = ['Painting', 'Cleaning', 'Gardening']
        search = 'Fix'
        response_html = self.request_html(search=search, categories=categories, sort_alpha=False, _reversed=True)

        self.assert_projects_in_response_ordered(response_html, [5, 2, 1])

    def test_search_sort_alpha_reverse(self):
        categories = ['Painting', 'Cleaning', 'Gardening']
        search = 'Fix'
        response_html = self.request_html(search=search, categories=categories, sort_alpha=True, _reversed=True)

        self.assert_projects_in_response_ordered(response_html, [1, 2, 5])

    def test_category_1(self):
        categories = ['Cleaning']
        response_html = self.request_html(categories=categories)

        self.assert_projects_in_response(response_html, {5})

    def test_category_2(self):
        categories = ['Cleaning', 'Painting']
        response_html = self.request_html(categories=categories)

        self.assert_projects_in_response(response_html, {2, 3, 4, 5, 6})

    def test_category_invalid_name(self):
        """
        Invalid category names should be ignored, and return results of valid categories.
        """
        categories = ['Cleaning', 'Pa1tgsing 13']
        response_html = self.request_html(categories=categories)

        self.assert_projects_in_response(response_html, {5})

    def test_invalid_keys(self):
        """
        Invalid keys should be ignored, and return results depending on valid keys.
        """
        categories = ['Painting', 'Cleaning', 'Gardening']
        form_data = {
            'advanced_search_form': '',
            'search': 'Fix',
            'some random keys': 'value test',
            'camelCaseKey': 'testAgain',
            'just_another': 'okay',
        }
        if categories:
            for category in categories:
                form_data['filter-category-' + category] = ''

        response_html = self.client.post(self.url, form_data).content.decode('utf-8')

        self.assert_projects_in_response_ordered(response_html, [1, 2, 5])

    def request_html(self, search='', categories=None, sort_alpha=True, _reversed=True):
        return self.request(search, categories, sort_alpha, _reversed).content.decode('utf-8').strip()

    def request(self, search='', categories=None, sort_alpha=True, _reversed=True):
        form_data = self.get_form_data(search=search, categories=categories, sort_alpha=sort_alpha, _reversed=_reversed)
        return self.client.post(self.url, form_data)

    @staticmethod
    def get_form_data(search='', categories=None, sort_alpha=False, _reversed=False):
        form_data = {
            'advanced_search_form': '',
            'search': search,
        }
        if categories:
            for category in categories:
                form_data['filter-category-' + category] = ''
        if sort_alpha:
            form_data['sort-alpha'] = ''
        if _reversed:
            form_data['reversed'] = ''
        return form_data

    @staticmethod
    def get_project_ids_in_html(html):
        project_ids = []
        lst = html.split('href="/projects/')
        for substring in lst[1:]:
            project_ids.append(int(substring.split('/')[0]))
        return project_ids

    def assert_projects_in_response(self, response_html, expected_projects):
        """
        Asserts the given project id's are present in the given response_html
        """
        returned_projects = self.get_project_ids_in_html(response_html)
        self.assertEqual(set(returned_projects), expected_projects)

        # Assert no duplicates are returned
        self.assertEqual(list(dict.fromkeys(returned_projects)), returned_projects)

    def assert_projects_in_response_ordered(self, response_html, expected_projects):
        """
        Asserts the given project id's are present and ordered as in the given response_html
        """
        returned_projects = self.get_project_ids_in_html(response_html)
        self.assertEqual(returned_projects, expected_projects)

        # Assert no duplicates are returned
        self.assertEqual(list(dict.fromkeys(returned_projects)), returned_projects)


class NewProjectTests(TestCase):
    fixtures = ['test_seed_standard.json']

    def setUp(self):
        self.client = Client()
        self.url = reverse(projects.views.new_project)

    def test_handle_valid_form_add_project(self):
        self.login("joe")
        self.post()
        self.assertTrue(Project.objects.get(title='.test title'))

    def test_handle_valid_form_add_project_with_tasks(self):
        self.login("joe")
        tasks = {
            'task_title': ['.test task title', '.test task title', '.test task title'],
            'task_description': ['test', 'test', 'test'],
            'task_budget': [1, 2, 3]
        }
        self.post(tasks=tasks)
        project = Project.objects.filter(title='.test title').first()
        self.assertTrue(project)
        self.assertEqual(project.tasks.all().count(), 3)
        self.assertEqual(Task.objects.filter(title='.test task title').count(), 3)

    def test_handle_invalid_form(self):
        self.login("joe")
        self.post(category_id=154)
        with self.assertRaises(Project.DoesNotExist):
            Project.objects.get(title='.test title')

    def login(self, username, password="qwerty123"):
        self.client.login(username=username, password=password)

    def post(self, category_id=1, tasks=None):
        post_form = {
            'title': '.test title',
            'description': 'description',
            'category_id': category_id,
        }
        if tasks is not None:
            post_form.update(tasks)

        return self.client.post(self.url, post_form, follow=True)


class ProjectViewTests(TestCase):
    fixtures = ['test_seed_standard.json']

    def setUp(self):
        self.project = Project.objects.get(pk=1)
        self.task = self.project.tasks.all()[0]

        self.client = Client()
        self.url = reverse(projects.views.upload_file_to_task, args=[self.project.id, self.task.id])

        self.task = Task.objects.create(project=self.project, title='test new task', description='test description',
                                        budget=123)
        self.user = User.objects.get(username='joe')
        self.client = Client()

    def test_user_submit_offer(self):
        self.login('joe')
        url = reverse(projects.views.project_view, args=[self.project.id])
        response = self.client.post(url, {
            'offer_submit': 'offer_submit',
            'title': self.task.title,
            'description': self.task.description,
            'price': self.task.budget,
            'taskvalue': self.task.id
        }, follow=True)
        self.assertEqual(response.context['project'].id, self.project.id)

    def test_project_owner_status_change(self):
        self.login('admin')
        url = reverse(projects.views.project_view, args=[self.project.id])

        self.assertEqual(Notification.objects.all().count(), 0)

        response = self.client.post(url, {'status_change': '1', 'status': 'i'})
        self.assertEqual(response.context['project'].id, self.project.id)
        self.assertEqual(Notification.objects.all().count(), 1)

    def test_project_owner_offer_accept_response(self):
        task_offer = TaskOffer.objects.create(task=self.task, title='testTaskOffer', offerer=self.user.profile,
                                              feedback='testFeedback', status='p')

        self.login('admin')
        url = reverse(projects.views.project_view, args=[self.project.id])

        response = self.client.post(url, {'offer_response': '1', 'taskofferid': task_offer.id,
                                          'status': 'a', 'feedback': 'taskOfferFeedback'})
        self.assertEqual(response.context['project'].id, self.project.id)

    def login(self, username, password="qwerty123"):
        self.client.login(username=username, password=password)


class AcceptOfferTests(TestCase):
    def setUp(self):
        user = User.objects.create_user(username='testUser', password='12345')
        self.project_owner = User.objects.create_user(username='testProjectOwner', password='12345')

        category = ProjectCategory.objects.create(name='testProjectCategory')
        project = Project.objects.create(user=self.project_owner.profile, title='testProject',
                                         description='testDescription', category=category, status='o')
        task = Task.objects.create(project=project, title='testTask', description='testDescription')
        self.task_offer = TaskOffer.objects.create(task=task, title='testTaskOffer', offerer=user.profile,
                                                   feedback='testUserFeedback')

        self.client = Client()

        self.client.login(username='testProjectOwner', password='12345')
        self.url = reverse(projects.views.project_view, args=[project.id])

        random_string = ''.join(random.choices(string.ascii_uppercase + string.digits, k=100))
        self.feedback = 'testProjectOwnerFeedback ' + random_string

    def test_accept_offer(self):
        self.handle_offer('a')

    def test_pending_offer(self):
        self.handle_offer('p')

    def test_decline_offer(self):
        self.handle_offer('d')

    def handle_offer(self, status):
        self.client.post(self.url, {
            'offer_response': '1',
            'taskofferid': self.task_offer.id, 'status': status,
            'feedback': self.feedback,
        })

        updated_task_offer = TaskOffer.objects.get(id=self.task_offer.id)
        self.assertEqual(updated_task_offer.status, status)
        self.assertEqual(updated_task_offer.feedback, self.feedback)


class UploadFileToTaskTests(TestCase):
    fixtures = ['test_seed_standard.json']

    def setUp(self):
        # A file name that should not collide with other existing static upload files.
        self.file_name = '.test'

        self.project = Project.objects.get(pk=1)
        self.task = self.project.tasks.all()[0]
        self.team = self.task.teams.all()[0]

        self.client = Client()
        self.url = reverse(projects.views.upload_file_to_task, args=[self.project.id, self.task.id])

    def tearDown(self):
        file_path = 'static/uploads/tasks/1/' + self.file_name
        if os.path.exists(file_path):
            os.remove(file_path)

    def test_handle_task_file_form(self):
        self.login("admin")
        file = SimpleUploadedFile(self.file_name, b'random content')

        self.assertFalse(self.task.files.exists())

        response = self.post(file)

        self.assertRedirects(response, "/projects/1/tasks/1/")
        self.assertEqual(self.task.files.all().count(), 1)

    def test_handle_task_file_form_no_permission_redirect(self):
        self.login("random_user")
        file = SimpleUploadedFile(self.file_name, b'random content')

        self.assertFalse(self.task.files.exists())

        response = self.post(file)

        self.assertRedirects(response, "/user/login/")
        self.assertFalse(self.task.files.exists())

    def test_handle_task_file_form_team_user(self):
        # Need an existing file with modify permission for a team member to replace a file
        file = SimpleUploadedFile(self.file_name, b'random content')
        task_file = TaskFile.objects.create(task=self.task, file=file)
        TaskFileTeam.objects.create(file=task_file, team=self.team, name='test team file', modify=True)

        file_in_db = self.task.teams.first().file.first().file.file

        content = self.read_file(file_in_db.name)

        self.login("team_user")
        file = SimpleUploadedFile(self.file_name, b'new random content')

        response = self.post(file)

        self.assertRedirects(response, "/projects/1/tasks/1/")

        new_content = self.read_file(file_in_db.name)
        self.assertNotEqual(content, new_content)
        self.assertEqual(new_content, ['new random content'])

    @staticmethod
    def read_file(path):
        with open(path, 'r') as file:
            return file.readlines()

    def login(self, username, password="qwerty123"):
        self.client.login(username=username, password=password)

    def post(self, file):
        return self.client.post(self.url, {'file': file}, follow=True)


class GetUserTaskPermissionsTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testUser', password='12345')
        self.project_owner = User.objects.create_user(username='testProjectOwner', password='12345')
        category = ProjectCategory.objects.create(name='testProjectCategory')
        self.project = Project.objects.create(user=self.project_owner.profile, title='testProject',
                                              description='testDescription', category=category, status='o')
        self.task = Task.objects.create(project=self.project, title='testTask', description='testDescription',
                                        budget=12345)

    def test_user_is_project_owner(self):
        result = get_user_task_permissions(self.project_owner, self.task)
        self.assertEqual(result, {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        })

    def test_user_is_offerer_of_accepted_task(self):
        TaskOffer.objects.create(task=self.task, title='testTaskOffer', offerer=self.user.profile,
                                 feedback='testFeedback', status='a')
        result = get_user_task_permissions(self.user, self.task)
        self.assertEqual(result, {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        })

    def test_other_users(self):
        result = get_user_task_permissions(self.user, self.task)
        self.assertEqual(result, {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        })


class TaskViewTests(TestCase):
    fixtures = ['test_seed_standard.json']

    def setUp(self):
        # A file name that should not collide with other existing static upload files.
        self.file_name = '.test'

        self.project = Project.objects.get(pk=1)
        self.task = self.project.tasks.all()[0]

        self.client = Client()
        self.url = reverse(projects.views.task_view, args=[self.project.id, self.task.id])

        self.other_user = User.objects.get(username='random_user')
        self.team_user = User.objects.get(username='team_user')

    def tearDown(self):
        file_path = 'static/uploads/tasks/1/' + self.file_name
        if os.path.exists(file_path):
            os.remove(file_path)

    def test_handle_delivery_form(self):
        self.login("joe")
        file = SimpleUploadedFile(self.file_name, b'random content')

        self.assertEqual(self.task.status, Task.AWAITING_DELIVERY)
        self.assertEqual(self.task.delivery.all().count(), 0)

        self.post('delivery', {
            'comment': 'Done all requirements. Hope you like it.',
            'file': file
        })

        self.assertEqual(self.task.delivery.all().count(), 1)

        self.task.refresh_from_db()
        self.assertEqual(self.task.status, Task.PENDING_ACCEPTANCE)

    def test_handle_delivery_form_no_permission_redirect(self):
        self.login("random_user")
        file = SimpleUploadedFile(self.file_name, b'random content')

        self.assertEqual(self.task.status, Task.AWAITING_DELIVERY)
        self.assertEqual(self.task.delivery.all().count(), 0)

        response = self.post('delivery', {
            'comment': 'Done all requirements. Hope you like it.',
            'file': file
        })

        self.assertRedirects(response, "/user/login/")
        self.assertEqual(self.task.delivery.all().count(), 0)
        self.task.refresh_from_db()
        self.assertEqual(self.task.status, Task.AWAITING_DELIVERY)

    def test_handle_team_form(self):
        self.login("joe")
        self.assertFalse(Team.objects.filter(name='new_best_team').exists())
        self.post('team', {'name': 'new_best_team'})
        self.assertTrue(Team.objects.filter(name='new_best_team').exists())

    def test_handle_team_form_no_permission_redirect(self):
        self.assertFalse(Team.objects.filter(name='new_best_team').exists())
        self.post('team', {'name': 'new_best_team'})
        self.assertFalse(Team.objects.filter(name='new_best_team').exists())

    def test_handle_team_add_form(self):
        self.login("joe")
        team = Team.objects.create(name='new_best_team', task=self.task)

        self.assertEqual(team.members.all().count(), 0)

        self.post('team-add', {
            'team-id': team.id,
            'members': [self.other_user.id,
                        self.team_user.id]
        })

        team.refresh_from_db()
        self.assertEqual(team.members.all().count(), 2)

    def test_update_task_team_file_permissions(self):
        self.login("joe")
        team = Team.objects.create(name='new_best_team', task=self.task)
        file = SimpleUploadedFile(self.file_name, b'random content')
        task_file = TaskFile.objects.create(task=self.task, file=file)
        tft = TaskFileTeam.objects.create(file=task_file, team=team)

        key_tft_id = 'permission-perobj-' + str(task_file.id) + '-' + str(team.id)
        permission = {
            'write': 'permission-write-' + str(task_file.id) + '-' + str(team.id),
            'read': 'permission-read-' + str(task_file.id) + '-' + str(team.id),
            'modify': 'permission-modify-' + str(task_file.id) + '-' + str(team.id),
            'upload': 'permission-upload-' + str(team.id),
        }

        self.assertFalse(tft.read)
        self.assertFalse(tft.write)
        self.assertFalse(tft.modify)
        self.assertFalse(team.write)

        self.post('permissions', {key_tft_id: tft.id, permission['write']: True})
        tft.refresh_from_db()
        self.assertTrue(tft.write)

        self.post('permissions', {key_tft_id: tft.id, permission['read']: True, permission['modify']: True})
        tft.refresh_from_db()
        self.assertTrue(tft.read)
        self.assertTrue(tft.modify)

        self.post('permissions', {key_tft_id: tft.id, permission['write']: False, permission['upload']: True})
        tft.refresh_from_db()
        team.refresh_from_db()
        self.assertTrue(team.write)
        self.assertFalse(tft.write)

    def test_update_task_team_file_permissions_invalid_team(self):
        self.login("joe")
        team = Team.objects.create(name='new_best_team', task=self.task)
        file = SimpleUploadedFile(self.file_name, b'random content')
        task_file = TaskFile.objects.create(task=self.task, file=file)
        tft = TaskFileTeam.objects.create(file=task_file, team=team)

        non_existing_team_id = 123

        key_tft_id = 'permission-perobj-' + str(task_file.id) + '-' + str(non_existing_team_id)
        permission = {
            'write': 'permission-write-' + str(task_file.id) + '-' + str(non_existing_team_id),
            'read': 'permission-read-' + str(task_file.id) + '-' + str(non_existing_team_id),
            'modify': 'permission-modify-' + str(task_file.id) + '-' + str(non_existing_team_id),
            'upload': 'permission-upload-' + str(non_existing_team_id),
        }

        self.assertFalse(tft.write)
        self.assertFalse(team.write)

        self.post('permissions', {key_tft_id: tft.id, permission['upload']: True})
        tft.refresh_from_db()

        self.assertFalse(tft.write)
        self.assertFalse(team.write)

    def test_update_task_team_file_permissions_invalid_file(self):
        self.login("joe")
        team = Team.objects.create(name='new_best_team', task=self.task)
        file = SimpleUploadedFile(self.file_name, b'random content')
        task_file = TaskFile.objects.create(task=self.task, file=file)
        tft = TaskFileTeam.objects.create(file=task_file, team=team)

        non_existing_file_id = 123

        key_tft_id = 'permission-perobj-' + str(non_existing_file_id) + '-' + str(team.id)
        permission = {
            'write': 'permission-write-' + str(non_existing_file_id) + '-' + str(team.id),
            'read': 'permission-read-' + str(non_existing_file_id) + '-' + str(team.id),
            'modify': 'permission-modify-' + str(non_existing_file_id) + '-' + str(team.id),
            'upload': 'permission-upload-' + str(non_existing_file_id),
        }

        self.assertFalse(tft.write)
        self.assertFalse(team.write)

        self.post('permissions', {key_tft_id: tft.id, permission['upload']: True, permission['write']: True})
        tft.refresh_from_db()

        self.assertFalse(tft.write)
        self.assertFalse(team.write)

    def test_handle_delivery_response_form_accept(self):
        self.login('admin')

        project_owner = User.objects.get(username='joe')
        delivery = Delivery.objects.create(delivery_user=project_owner.profile, task=self.task)

        self.assertEqual(delivery.status, Delivery.PENDING)
        self.assertEqual(self.task.status, Task.AWAITING_DELIVERY)

        self.post('delivery-response', {
            'delivery-id': delivery.id,
            'feedback': 'something',
            'status': Delivery.ACCEPTED
        })

        delivery.refresh_from_db()
        self.task.refresh_from_db()
        self.assertEqual(delivery.status, Delivery.ACCEPTED)
        self.assertEqual(self.task.status, Task.PENDING_PAYMENT)

    def test_handle_delivery_response_form_decline(self):
        self.login('admin')

        project_owner = User.objects.get(username='joe')
        delivery = Delivery.objects.create(delivery_user=project_owner.profile, task=self.task)

        self.assertEqual(delivery.status, Delivery.PENDING)
        self.assertEqual(self.task.status, Task.AWAITING_DELIVERY)

        self.post('delivery-response', {
            'delivery-id': delivery.id,
            'feedback': 'something',
            'status': Delivery.DECLINED
        })

        delivery.refresh_from_db()
        self.task.refresh_from_db()
        self.assertEqual(delivery.status, Delivery.DECLINED)
        self.assertEqual(self.task.status, Task.DECLINED_DELIVERY)

    @skip("The will fail, since the original implementation do not cover this case."
          "A user should not accept its own delivery.")
    def test_handle_delivery_response_form_accept_security(self):
        self.login('joe')

        project_owner = User.objects.get(username='joe')
        delivery = Delivery.objects.create(delivery_user=project_owner.profile, task=self.task)

        self.assertEqual(delivery.status, Delivery.PENDING)
        self.assertEqual(self.task.status, Task.AWAITING_DELIVERY)

        self.post('delivery-response', {
            'delivery-id': delivery.id,
            'feedback': 'something',
            'status': Delivery.ACCEPTED
        })

        delivery.refresh_from_db()
        self.task.refresh_from_db()
        self.assertEqual(delivery.status, Delivery.PENDING)
        self.assertEqual(self.task.status, Task.AWAITING_DELIVERY)

    def login(self, username, password="qwerty123"):
        self.client.login(username=username, password=password)

    def post(self, form_name, form_data):
        post_form = {
            form_name: form_name
        }
        post_form.update(form_data)

        return self.client.post(self.url, post_form, follow=True)


class TaskPermissionTests(TestCase):
    fixtures = ['test_seed_standard.json']

    def setUp(self):
        self.project = Project.objects.get(pk=1)
        self.task = self.project.tasks.all()[0]

        self.client = Client()
        self.client.login(username='admin', password='qwerty123')
        self.url = reverse(projects.views.task_permissions, args=[self.project.id, self.task.id])
        self.other_user = User.objects.get(username='random_user')

    def test_give_user_read_permission(self):
        self.helper_test_give_user_permission(self.other_user, 'Read')

    def test_give_user_write_permission(self):
        self.helper_test_give_user_permission(self.other_user, 'Write')

    def test_give_user_modify_permission(self):
        self.helper_test_give_user_permission(self.other_user, 'Modify')

    def helper_test_give_user_permission(self, user, permission):
        form_data = {
            'user': user.id,
            'permission': permission,
        }
        self.assertFalse(self.is_user_in_permission_set(user, permission))
        self.client.post(self.url, form_data)
        self.assertTrue(self.is_user_in_permission_set(user, permission))

    def is_user_in_permission_set(self, user, permission):
        permissions = {
            'Read': self.task.read.all(),
            'Write': self.task.write.all(),
            'Modify': self.task.modify.all(),
        }
        return permissions[permission].filter(user=user).exists()
