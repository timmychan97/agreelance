from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.core import mail
from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404

import notification
from favorites.models import Favorite
from user.models import Profile
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, directory_path
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm, \
    TaskPermissionForm, DeliveryForm, TaskDeliveryResponseForm, TeamForm, TeamAddForm


def projects(request):
    return render(request, 'projects/projects.html',
                  {
                      'projects': Project.objects.all(),
                      'project_categories': ProjectCategory.objects.all(),
                  })


def projects_with_filter(request):
    if request.method == 'POST' and 'advanced_search_form' in request.POST:
        response = render(request, "projects/project_cards.html", {
            'projects': get_projects_by_advanced_search_form(request),
        })
    else:
        # Not accepting GET requests on purpose
        response = HttpResponseNotFound()
    return response


def get_projects_by_advanced_search_form(request):
    result_projects = Project.objects.all()

    if "search" in request.POST.keys() and request.POST['search'] != "":
        projects_by_search = get_projects_by_search(request.POST['search'])
        result_projects = result_projects.intersection(projects_by_search)

    category_names = get_category_names_by_advanced_search_form(request)
    if category_names:
        projects_by_categories = get_projects_by_category_names(category_names)
        result_projects = result_projects.intersection(projects_by_categories)

    if "sort-alpha" in request.POST.keys():
        result_projects = result_projects.order_by('title')
    else:
        result_projects = result_projects.order_by('id')

    list_result_projects = list(result_projects)
    if "reversed" in request.POST.keys():
        list_result_projects.reverse()

    return list_result_projects


def get_category_names_by_advanced_search_form(request):
    return list(map(lambda x: x[16:], filter(lambda x: x.startswith('filter-category-'), request.POST)))


def get_projects_by_category_names(category_names):
    project_qs = Project.objects.none()
    for category_name in category_names:
        project_qs = project_qs.union(Project.objects.filter(category__name=category_name))
    return project_qs


def get_projects_by_search(search_string):
    return Project.objects.filter(title__icontains=search_string)


@login_required
def new_project(request):
    if request.method == 'POST':
        project_form = ProjectForm(request.POST)
        if project_form.is_valid():
            project = add_project(request, project_form)
            send_email_to_category_subscribers_about_new_project(request, project)
            add_tasks_to_project(request, project)
            return redirect('project_view', project_id=project.id)
    else:
        project_form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': project_form})


def add_project(request, project_form):
    project = project_form.save(commit=False)
    project.user = request.user.profile
    project.category = get_object_or_404(ProjectCategory, id=request.POST.get('category_id'))
    project.save()
    return project


def send_email_to_category_subscribers_about_new_project(request, project):
    people = Profile.objects.filter(categories__id=project.category.id)
    for person in people:
        if person.user.email:
            try:
                with mail.get_connection() as connection:
                    mail.EmailMessage(
                        "New Project: " + project.title,
                        "A new project you might be interested in was created and can be viwed at " +
                        get_current_site(request).domain + '/projects/' + str(project.id),
                        "Agreelancer", [person.user.email], connection=connection,
                    ).send()
            except Exception as e:
                messages.success(request, 'Sending of email to ' + person.user.email + " failed: " + str(e))


def add_tasks_to_project(request, project):
    task_title = request.POST.getlist('task_title')
    task_description = request.POST.getlist('task_description')
    task_budget = request.POST.getlist('task_budget')
    for i in range(len(task_title)):
        Task.objects.create(
            title=task_title[i],
            description=task_description[i],
            budget=task_budget[i],
            project=project,
        )


def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()

    total_budget = get_total_budget(tasks)
    is_project_favorited = is_project_favorited_by_user(request.user, project)

    if is_project_owner(request.user, project):
        if request.method == 'POST':
            if 'offer_response' in request.POST:
                handle_offer_response_form(request)
            if 'status_change' in request.POST:
                handle_status_change_form(request, project)

        return render(request, 'projects/project_view.html', {
            'project': project,
            'tasks': tasks,
            'status_form': ProjectStatusForm(initial={'status': project.status}),
            'total_budget': total_budget,
            'offer_response_form': TaskOfferResponseForm(),
            'is_favorited': is_project_favorited,
        })
    else:
        if request.method == 'POST' and 'offer_submit' in request.POST:
            handle_task_offer_form(request)

        return render(request, 'projects/project_view.html', {
            'project': project,
            'tasks': tasks,
            'task_offer_form': TaskOfferForm(),
            'total_budget': total_budget,
            'is_favorited': is_project_favorited,
        })


def handle_task_offer_form(request):
    task_offer_form = TaskOfferForm(request.POST)
    if task_offer_form.is_valid():
        add_task_offer_to_task(request, task_offer_form)


def add_task_offer_to_task(request, task_offer_form):
    task_offer = task_offer_form.save(commit=False)
    task_offer.task = Task.objects.get(pk=request.POST.get('taskvalue'))
    task_offer.offerer = request.user.profile
    task_offer.save()


def handle_offer_response_form(request):
    task_offer = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
    offer_response_form = TaskOfferResponseForm(request.POST, instance=task_offer)
    if offer_response_form.is_valid():
        offer_response = offer_response_form.save(commit=False)

        if offer_response.status == TaskOffer.ACCEPTED:
            handle_accept_task_offer(offer_response)

        offer_response.save()


def handle_accept_task_offer(offer_response):
    offer_response.task.read.add(offer_response.offerer)
    offer_response.task.write.add(offer_response.offerer)
    project = offer_response.task.project
    project.participants.add(offer_response.offerer)


def handle_status_change_form(request, project):
    status_form = ProjectStatusForm(request.POST)
    if status_form.is_valid():
        update_project_status(project, status_form)
        send_notification_to_project_participants(project, "Status of the project has been changed")


def update_project_status(project, status_form):
    project_status = status_form.save(commit=False)
    project.status = project_status.status
    project.save()


def is_project_favorited_by_user(user, project):
    if user.is_authenticated:
        return Favorite.objects.filter(project=project, user=user.profile).exists()
    return False


def get_total_budget(tasks):
    total_budget = 0
    for item in tasks:
        total_budget += item.budget
    return total_budget


def send_notification_to_project_participants(project, description="New notification for a project"):
    participants = project.participants.all()
    notification.views.create_notifications(participants, description, '/projects/' + str(project.id))


def is_project_owner(user, project):
    return user == project.user.user


def is_accepted_task_offerer(user, task):
    accepted_task_offer = task.accepted_task_offer()
    return accepted_task_offer and accepted_task_offer.offerer == user.profile


@login_required
def upload_file_to_task(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    user_task_permissions = get_user_task_permissions(request.user, task)

    if not (user_task_permissions['modify'] or user_task_permissions['write'] or
            user_task_permissions['upload']):
        return redirect('/user/login')

    if request.method == 'POST':
        task_file_form = TaskFileForm(request.POST, request.FILES)
        if task_file_form.is_valid():
            handle_valid_task_file_form(request, task_file_form, task, user_task_permissions)
            return redirect('task_view', project_id=project_id, task_id=task_id)

    return render(
        request,
        'projects/upload_file_to_task.html',
        {
            'project': project,
            'task': task,
            'task_file_form': TaskFileForm(),
        }
    )


def handle_valid_task_file_form(request, task_file_form, task, user_task_permissions):
    task_file = task_file_form.save(commit=False)
    task_file.task = task
    existing_file = task.files.filter(file=directory_path(task_file, task_file.file.file)).first()

    if not has_access(request.user, user_task_permissions, existing_file):
        messages.warning(request, "You do not have access to modify this file")
        return

    task_file.save()
    delete_file_if_exist(existing_file)

    if not (is_project_owner(request.user, task.project) or
            is_accepted_task_offerer(request.user, task)):
        teams = request.user.profile.teams.filter(task__id=task.id)
        add_task_file_to_teams(task_file, teams)


def has_access(user, user_task_permissions, file):
    access = user_task_permissions['modify'] or user_task_permissions['owner']
    for team in user.profile.teams.all():
        file_modify_access = TaskFileTeam.objects.filter(team=team, file=file,
                                                         modify=True).exists()
        access = access or file_modify_access
    return access


def delete_file_if_exist(file):
    if file:
        file.delete()


def add_task_file_to_teams(task_file, teams):
    for team in teams:
        tft = TaskFileTeam()
        tft.team = team
        tft.file = task_file
        tft.read = True
        tft.save()


def get_user_task_permissions(user, task):
    if is_project_owner(user, task.project):
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }
    if is_accepted_task_offerer(user, task):
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }
    user_permissions = {'write': user.profile.task_participants_write.filter(id=task.id).exists(),
                        'read': user.profile.task_participants_read.filter(id=task.id).exists(),
                        'modify': user.profile.task_participants_modify.filter(id=task.id).exists(),
                        'owner': False,
                        # Team members can view its teams tasks
                        'view_task': user.profile.teams.filter(task__id=task.id).exists(),
                        'upload': user.profile.teams.filter(task__id=task.id, write=True).exists()}

    return user_permissions


@login_required
def task_view(request, project_id, task_id):
    task = Task.objects.get(pk=task_id)

    user_task_permissions = get_user_task_permissions(request.user, task)
    if not (user_task_permissions['read'] or
            user_task_permissions['write'] or
            user_task_permissions['modify'] or
            user_task_permissions['owner'] or
            user_task_permissions['view_task']):
        return redirect('/user/login')

    if request.method == 'POST':
        if is_accepted_task_offerer(request.user, task):
            if 'delivery' in request.POST:
                handle_delivery_form(request, task)
            if 'team' in request.POST:
                handle_team_form(request, task)
            if 'team-add' in request.POST:
                handle_team_add_form(request)
            if 'permissions' in request.POST:
                update_task_team_file_permissions(request, task)
        if 'delivery-response' in request.POST:
            handle_delivery_response_form(request, task)
    return render_task_view(request, task, project_id)


def render_task_view(request, task, project_id):
    team_files = []
    per = {}
    for file in task.files.all():
        per[file.name()] = {}
        for task_file_team in file.teams.all():
            per[file.name()][task_file_team.team.name] = task_file_team
            if task_file_team.read:
                team_files.append(task_file_team)
    return render(request, 'projects/task_view.html', {
        'task': task,
        'project': Project.objects.get(pk=project_id),
        'user_permissions': get_user_task_permissions(request.user, task),
        'deliver_form': DeliveryForm(),
        'deliveries': task.delivery.all(),
        'deliver_response_form': TaskDeliveryResponseForm(),
        'team_form': TeamForm(),
        'team_add_form': TeamAddForm(),
        'team_files': team_files,
        'per': per
    })


def handle_delivery_form(request, task):
    deliver_form = DeliveryForm(request.POST, request.FILES)
    if deliver_form.is_valid():
        add_delivery_to_task(request.user, deliver_form, task)


def add_delivery_to_task(user, deliver_form, task):
    delivery = deliver_form.save(commit=False)
    delivery.task = task
    delivery.delivery_user = user.profile
    delivery.save()
    task.status = Task.PENDING_ACCEPTANCE
    task.save()


def handle_delivery_response_form(request, task):
    instance = get_object_or_404(Delivery, id=request.POST.get('delivery-id'))
    deliver_response_form = TaskDeliveryResponseForm(request.POST, instance=instance)
    if deliver_response_form.is_valid():
        update_delivery_with_response(request, deliver_response_form, task)


def update_delivery_with_response(request, deliver_response_form, task):
    delivery = deliver_response_form.save(commit=False)
    delivery.responding_time = timezone.now()
    delivery.responding_user = request.user.profile
    delivery.save()
    update_task_status(task, delivery)


def update_task_status(task, delivery):
    if delivery.status == Delivery.ACCEPTED:
        task.status = Task.PENDING_PAYMENT
        task.save()
    elif delivery.status == Delivery.DECLINED:
        task.status = Task.DECLINED_DELIVERY
        task.save()


def handle_team_form(request, task):
    team_form = TeamForm(request.POST)
    if team_form.is_valid():
        add_team_to_task(team_form, task)


def add_team_to_task(team_form, task):
    team = team_form.save(False)
    team.task = task
    team.save()


def handle_team_add_form(request):
    instance = get_object_or_404(Team, id=request.POST.get('team-id'))
    team_add_form = TeamAddForm(request.POST, instance=instance)
    if team_add_form.is_valid():
        add_users_to_team(team_add_form)


def add_users_to_team(team_add_form):
    team = team_add_form.save(commit=False)
    team.members.add(*team_add_form.cleaned_data['members'])
    team.save()


def update_task_team_file_permissions(request, task):
    for team in task.teams.all():
        for file in task.files.all():
            selected_file_permissions = get_file_permissions_from_post_request(request.POST, team, file)
            try:
                tft_id = get_task_file_team_id_from_post_request(request.POST, team, file)
                instance = TaskFileTeam.objects.get(id=tft_id)
            except TaskFileTeam.DoesNotExist:
                instance = TaskFileTeam(file=file, team=team)

            instance.read = selected_file_permissions['read']
            instance.write = selected_file_permissions['write']
            instance.modify = selected_file_permissions['modify']
            instance.save()
        team.write = get_team_upload_permission_from_post_request(request.POST, team)
        team.save()


def get_file_permissions_from_post_request(post_data, team, file):
    return {
        'write': post_data.get('permission-write-' + str(file.id) + '-' + str(team.id)) or False,
        'read': post_data.get('permission-read-' + str(file.id) + '-' + str(team.id)) or False,
        'modify': post_data.get('permission-modify-' + str(file.id) + '-' + str(team.id)) or False,
        'upload': post_data.get('permission-upload-' + str(team.id)) or False,
    }


def get_task_file_team_id_from_post_request(post_data, team, file):
    return post_data.get('permission-perobj-' + str(file.id) + '-' + str(team.id))


def get_team_upload_permission_from_post_request(post_data, team):
    return post_data.get('permission-upload-' + str(team.id)) or False


@login_required
def task_permissions(request, project_id, task_id):
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)

    if int(project_id) != task.project.id or not (is_project_owner(request.user, project) or
                                                  is_accepted_task_offerer(request.user, task)):
        return redirect('task_view', project_id=project_id, task_id=task_id)

    if request.method == 'POST':
        task_permission_form = TaskPermissionForm(request.POST)
        if task_permission_form.is_valid():
            try:
                add_user_task_permission_to_task(task_permission_form, task)
            except User.DoesNotExist:
                print("User not found")
            return redirect('task_view', project_id=project_id, task_id=task_id)

    return render(
        request,
        'projects/task_permissions.html',
        {
            'project': project,
            'task': task,
            'form': TaskPermissionForm(),
        }
    )


def add_user_task_permission_to_task(task_permission_form, task):
    username = task_permission_form.cleaned_data['user']
    user = User.objects.get(username=username)
    permission_type = task_permission_form.cleaned_data['permission']
    if permission_type == 'Read':
        task.read.add(user.profile)
    elif permission_type == 'Write':
        task.write.add(user.profile)
    elif permission_type == 'Modify':
        task.modify.add(user.profile)


@login_required
def delete_file(request, file_id):
    file = TaskFile.objects.get(pk=file_id)
    file.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
