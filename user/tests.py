from django.test import TestCase

from projects.models import ProjectCategory
from user.forms import SignUpForm


class SignUpFormBoundaryTests(TestCase):
    """
    Password and email fields are tested together on boundary, since they should match.
    """

    def setUp(self):
        ProjectCategory.objects.create(name='testCategory')
        ProjectCategory.objects.create(name='testCategory2')

        self.valid_form_data = {
            'username': "testUserName",
            'first_name': "testFirstName",
            'last_name': "testLastName",
            'categories': [2],
            'company': "testCompany",
            'email': "test@test.com",
            'email_confirmation': "test@test.com",
            'password1': "jqSpG2mx7byq1",
            'password2': "jqSpG2mx7byq1",
            'phone_number': "12345678",
            'country': "testCountry",
            'state': "testState",
            'city': "testCity",
            'postal_code': "testPostalCode",
            'street_address': "testStreetAddress"
        }

    def test_valid_form(self):
        self.assertTrue(SignUpForm(self.valid_form_data).is_valid())

    def test_username(self):
        self.string_length_boundary_test("username", "t" * 150)

    def test_first_name(self):
        self.string_length_boundary_test("first_name", "t" * 30)

    def test_last_name(self):
        self.string_length_boundary_test("last_name", "t" * 30)

    def test_categories_min(self):
        self.valid_form_data['categories'] = [1]
        self.assertTrue(SignUpForm(self.valid_form_data).is_valid())

    def test_categories_min_minus(self):
        self.valid_form_data['categories'] = [0]
        self.assertFalse(SignUpForm(self.valid_form_data).is_valid())

    def test_categories_empty(self):
        self.valid_form_data['categories'] = []
        self.assertFalse(SignUpForm(self.valid_form_data).is_valid())

    def test_categories_max(self):
        self.valid_form_data['categories'] = [ProjectCategory.objects.all().count()]
        self.assertTrue(SignUpForm(self.valid_form_data).is_valid())

    def test_categories_max_plus(self):
        self.valid_form_data['categories'] = [ProjectCategory.objects.all().count() + 1]
        self.assertFalse(SignUpForm(self.valid_form_data).is_valid())

    def test_company(self):
        self.string_length_boundary_test("company", "t" * 30, 0)

    def test_email_max(self):
        self.valid_form_data['email'] = "t" * (254 - 9) + "@test.com"
        self.valid_form_data['email_confirmation'] = "t" * (254 - 9) + "@test.com"
        self.assertTrue(SignUpForm(self.valid_form_data).is_valid())

    def test_email_max_plus(self):
        self.valid_form_data['email'] = "t" * (255 - 9) + "@test.com"
        self.valid_form_data['email_confirmation'] = "t" * (255 - 9) + "@test.com"
        self.assertFalse(SignUpForm(self.valid_form_data).is_valid())

    def test_email_min(self):
        self.valid_form_data['email'] = "x@y.zz"
        self.valid_form_data['email_confirmation'] = "x@y.zz"
        self.assertTrue(SignUpForm(self.valid_form_data).is_valid())

    def test_email_min_minus(self):
        self.valid_form_data['email'] = ""
        self.valid_form_data['email_confirmation'] = ""
        self.assertFalse(SignUpForm(self.valid_form_data).is_valid())

    def test_phone_number(self):
        self.string_length_boundary_test("phone_number", "t" * 50)

    def test_country(self):
        self.string_length_boundary_test("country", "t" * 50)

    def test_state(self):
        self.string_length_boundary_test("state", "t" * 50)

    def test_city(self):
        self.string_length_boundary_test("city", "t" * 50)

    def test_postal_code(self):
        self.string_length_boundary_test("postal_code", "t" * 50)

    def test_street_address(self):
        self.string_length_boundary_test("street_address", "t" * 50)

    def test_password_min(self):
        self.valid_form_data['password1'] = "jqSpG2mx"
        self.valid_form_data['password2'] = "jqSpG2mx"
        self.assertTrue(SignUpForm(self.valid_form_data).is_valid())

    def test_password_min_minus(self):
        self.valid_form_data['password1'] = "jqSpG2m"
        self.valid_form_data['password2'] = "jqSpG2m"
        self.assertFalse(SignUpForm(self.valid_form_data).is_valid())

    def string_length_boundary_test(self, key, max_string, min_length=1):
        # Max+
        self.valid_form_data[key] = max_string + "x"
        self.assertFalse(SignUpForm(self.valid_form_data).is_valid())

        # Max
        self.valid_form_data[key] = max_string
        self.assertTrue(SignUpForm(self.valid_form_data).is_valid())

        # Min
        self.valid_form_data[key] = max_string[:min_length]
        self.assertTrue(SignUpForm(self.valid_form_data).is_valid())

        # Min-
        if min_length > 0:
            self.valid_form_data[key] = max_string[:min_length - 1]
            self.assertFalse(SignUpForm(self.valid_form_data).is_valid())


class SignUpFormTwoWayDomainTests(TestCase):
    """
    2-way domain testing with the following parameters:

    parameters = {
            'username': ["FancyGamer", "Cutter6123"],
            'first_name': ["Timmy", "Truc"],
            'last_name': ["Chan", "Phan"],
            'categories': [[1], [2], [3]],
            'company': ["PnDrawing Inc.", "Hackerspace"],
            'email': ["hello@pn.com", "x@y.zz"],
            'email_confirmation': ["hello@pn.com", "x@y.zz"],
            'password1': ["jqSpG2mx7byq1", "FancyGamer", naruto123],
            'password2': ["jqSpG2mx7byq1", "FancyGamer", naruto123],
            'phone_number': ["12345678", "98765412"],
            'country': ["Norway", "China"],
            'state': ["Trondelag", "Beijing"],
            'city': ["Trondheim", "Beijing"],
            'postal_code': ["7045", "00000"],
            'street_address': ["Sofia Pettersen 69", "Haidian, Tsinghua University"]
        }
    """

    def setUp(self):
        ProjectCategory.objects.create(name='Drawing')
        ProjectCategory.objects.create(name='Lawn Mowing')
        ProjectCategory.objects.create(name='Babysitting')

        pairwise_test_data_raw = [
            ["FancyGamer", "Timmy", "Chan", [1], "PnDrawing Inc.", "hello@pn.com", "hello@pn.com", "jqSpG2mx7byq1",
             "jqSpG2mx7byq1", "12345678", "Norway", "Trondelag", "Trondheim", "7045", "Sofia Pettersen 69", ],
            ["FancyGamer", "Truc", "Phan", [2], "Hackerspace", "x@y.zz", "x@y.zz", "FancyGamer", "FancyGamer",
             "98765412", "China", "Beijing", "Beijing", "0", "Haidian, Tsinghua University", ],
            ["FancyGamer", "Timmy", "Chan", [3], "PnDrawing Inc.", "hello@pn.com", "hello@pn.com", "naruto123",
             "naruto123", "12345678", "China", "Beijing", "Beijing", "0", "Sofia Pettersen 69", ],
            ["Cutter6123", "Truc", "Phan", [3], "PnDrawing Inc.", "x@y.zz", "x@y.zz", "jqSpG2mx7byq1", "FancyGamer",
             "12345678", "Norway", "Beijing", "Trondheim", "7045", "Haidian, Tsinghua University", ],
            ["Cutter6123", "Timmy", "Chan", [1], "Hackerspace", "hello@pn.com", "hello@pn.com", "FancyGamer",
             "naruto123", "12345678", "China", "Beijing", "Trondheim", "0", "Sofia Pettersen 69", ],
            ["Cutter6123", "Timmy", "Phan", [2], "PnDrawing Inc.", "hello@pn.com", "x@y.zz", "naruto123",
             "jqSpG2mx7byq1", "98765412", "Norway", "Trondelag", "Beijing", "0", "Sofia Pettersen 69", ],
            ["Cutter6123", "Timmy", "Phan", [2], "PnDrawing Inc.", "hello@pn.com", "x@y.zz", "jqSpG2mx7byq1",
             "naruto123", "98765412", "Norway", "Trondelag", "Beijing", "7045", "Sofia Pettersen 69", ],
            ["Cutter6123", "Timmy", "Chan", [3], "Hackerspace", "hello@pn.com", "hello@pn.com", "FancyGamer",
             "jqSpG2mx7byq1", "12345678", "China", "Trondelag", "Beijing", "0", "Sofia Pettersen 69", ],
            ["Cutter6123", "Truc", "Chan", [1], "PnDrawing Inc.", "x@y.zz", "hello@pn.com", "naruto123", "FancyGamer",
             "12345678", "China", "Beijing", "Trondheim", "0", "Haidian, Tsinghua University", ]
        ]
        self.pairwise_test_data = self.pairwise_test_data_converter(pairwise_test_data_raw)

    def test_pair_0(self):
        form = SignUpForm(self.pairwise_test_data[0])
        self.assertTrue(form.is_valid())

    def test_pair_1(self):
        """
        Fails covered:
         - Identical password and username
        """
        form = SignUpForm(self.pairwise_test_data[1])
        self.assertFalse(form.is_valid())

    def test_pair_2(self):
        """
        Fails covered:
         - Common password
        """
        form = SignUpForm(self.pairwise_test_data[2])
        self.assertFalse(form.is_valid())

    def test_pair_3(self):
        """
        Fails covered:
         - Password mismatch
         - Non-existent location (not implemented)
        """
        form = SignUpForm(self.pairwise_test_data[3])
        self.assertFalse(form.is_valid())

    def test_pair_4(self):
        """
        Fails covered:
         - Common password
         - Non-existent location (not implemented)
        """
        form = SignUpForm(self.pairwise_test_data[4])
        self.assertFalse(form.is_valid())

    def test_pair_5(self):
        """
        Fails covered:
         - Email mismatch
         - Password mismatch
         - Common password
         - Non-existent location (not implemented)
        """
        form = SignUpForm(self.pairwise_test_data[5])
        self.assertFalse(form.is_valid())

    def test_pair_6(self):
        """
        Fails covered:
         - Email mismatch
         - Password mismatch
         - Common password
         - Non-existent location (not implemented)
        """
        form = SignUpForm(self.pairwise_test_data[6])
        self.assertFalse(form.is_valid())

    def test_pair_7(self):
        """
        Fails covered:
         - Password mismatch
         - Non-existent location (not implemented)
        """
        form = SignUpForm(self.pairwise_test_data[7])
        self.assertFalse(form.is_valid())

    def test_pair_8(self):
        """
        Fails covered:
         - Email mismatch
         - Password mismatch
         - Common password
         - Non-existent location (not implemented)
        """
        form = SignUpForm(self.pairwise_test_data[8])
        self.assertFalse(form.is_valid())

    def pairwise_test_data_converter(self, lst):
        """
        Convert list to dictionary
        :param lst: list of ordered list of test data
        :return: list of dictionary of test data
        """
        pairwise_test_data = []
        for test_data in lst:
            pairwise_test_data.append({
                'username': test_data[0],
                'first_name': test_data[1],
                'last_name': test_data[2],
                'categories': test_data[3],
                'company': test_data[4],
                'email': test_data[5],
                'email_confirmation': test_data[6],
                'password1': test_data[7],
                'password2': test_data[8],
                'phone_number': test_data[9],
                'country': test_data[10],
                'state': test_data[11],
                'city': test_data[12],
                'postal_code': test_data[13],
                'street_address': test_data[14]
            })
        return pairwise_test_data
