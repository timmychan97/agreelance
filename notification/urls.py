from django.urls import path
from . import views

urlpatterns = [
    path('get_all/', views.get_all, name='get_all'),
    path('get_all_after_date/', views.get_all_after_date, name='get_all_after_date'),
    path('set_viewed/<uuid>', views.set_viewed, name='set_viewed'),
    path('click/<uuid>', views.click, name='click'),
]
