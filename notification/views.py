from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.shortcuts import redirect
from django.urls import NoReverseMatch
from django.utils import timezone
from django.template import loader
from django.http import JsonResponse, HttpResponse, HttpResponseNotFound, Http404, HttpResponseBadRequest

from notification.models import Notification


@login_required
def get_all(request):
    notifications = Notification.objects.filter(user=request.user.profile).order_by('created').reverse()

    return response(request, notifications)


@login_required
def get_all_after_date(request):
    dt = datetime.utcfromtimestamp(float(request.POST.get('timestamp')))
    dt_timestamp = timezone.make_aware(dt, timezone.utc)

    notifications = Notification.objects.filter(user=request.user.profile, created__gt=dt_timestamp)
    return response(request, notifications)


def response(request, notifications):
    t = loader.get_template('notification/notification_list.html')
    c = {'notifications': notifications}
    html = t.render(c, request)
    retrieved_at = datetime.timestamp(datetime.now())

    return JsonResponse({'retrievedAt': retrieved_at, 'html': html})


@login_required
def set_viewed(request, uuid):
    try:
        notification = Notification.objects.get(uuid=uuid)
        notification.viewed = True
        notification.save()
        return HttpResponse()
    except ObjectDoesNotExist:
        return HttpResponseNotFound("The notification doesn't exist.")
    except ValidationError:
        return HttpResponseBadRequest('Invalid UUID')


@login_required
def click(request, uuid):
    result = set_viewed(request, uuid)
    if result.status_code == 200:
        try:
            notification = Notification.objects.get(uuid=uuid)
            return redirect(notification.url)
        except NoReverseMatch:
            raise Http404('The redirection url in the notification is invalid.')
    else:
        return result


def create_notifications(user_profiles, description, url):
    for user in user_profiles:
        Notification.objects.create(user=user, description=description, url=url)
