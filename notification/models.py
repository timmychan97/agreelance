import uuid

from django.db import models
from user.models import Profile
from django.utils import timezone


# Create your models here.
class Notification(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=2047, blank=True, null=True)
    created = models.DateTimeField(editable=False, null=True)
    viewed = models.BooleanField(default=False)

    def __str__(self):
        return self.user.user.username + " " + self.description + " " + self.url

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()
        return super(Notification, self).save(*args, **kwargs)
