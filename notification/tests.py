from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse, NoReverseMatch

import notification
from notification.models import Notification
from notification.views import create_notifications


class NotificationTests(TestCase):
    fixtures = ['test_seed_standard.json']

    def setUp(self):
        self.client = Client()
        self.user = User.objects.get(username="joe").profile

    def test_send(self):
        create_notifications([self.user], "Test notification description", "/invalid/url/test.html")

        note = Notification.objects.filter(user=self.user)
        self.assertEqual(note.count(), 1)
        self.assertEqual(note[0].description, "Test notification description")
        self.assertEqual(note[0].url, "/invalid/url/test.html")
        self.assertEqual(note[0].viewed, False)

    def test_click(self):
        self.assertEqual(Notification.objects.filter(user=self.user).count(), 0)
        self.assertEqual(Notification.objects.filter(user=self.user, viewed=False).count(), 0)

        for i in range(3):
            create_notifications([self.user], str(i), "/projects/1")
        self.assertEqual(Notification.objects.filter(user=self.user).count(), 3)
        self.assertEqual(Notification.objects.filter(user=self.user, viewed=False).count(), 3)

        # Click on the first notification
        self.client.login(username="joe", password="qwerty123")
        note = Notification.objects.get(description="0")
        url = reverse(notification.views.click, args=[note.uuid])
        self.client.post(url, follow=True)

        self.assertEqual(Notification.objects.filter(user=self.user).count(), 3)
        self.assertEqual(Notification.objects.filter(user=self.user, viewed=False).count(), 2)
        self.assertEqual(Notification.objects.get(user=self.user, viewed=True), note)

    def test_click_invalid_notification_url(self):
        self.assertEqual(Notification.objects.filter(user=self.user).count(), 0)
        self.assertEqual(Notification.objects.filter(user=self.user, viewed=False).count(), 0)

        for i in range(3):
            create_notifications([self.user], str(i), "invalid url")

        # Click on the first notification
        self.client.login(username="joe", password="qwerty123")
        note = Notification.objects.get(description="0")

        self.assert_response(notification.views.click, args=[note.uuid], status=404)

    def test_click_invalid_notification_uuid(self):
        self.assertEqual(Notification.objects.filter(user=self.user).count(), 0)
        self.assertEqual(Notification.objects.filter(user=self.user, viewed=False).count(), 0)

        for i in range(3):
            create_notifications([self.user], str(i), "/projects/1")

        # Click on the first notification
        self.client.login(username="joe", password="qwerty123")

        # Invalid inputs uuid
        self.assert_response(notification.views.click, args=["uf%h82jal"], status=400)
        self.assert_response(notification.views.click, args=[["uf%h82jal", "f4gag3"]], status=400)
        self.assert_response(notification.views.click, args=[5467], status=400)

        # No args should not be allowed
        with self.assertRaises(NoReverseMatch):
            self.assert_response(notification.views.click, args=[], status=400)

    def assert_response(self, view, args=None, status=200):
        url = reverse(view, args=args)
        response = self.client.post(url, follow=True)
        self.assertEqual(response.status_code, status)
