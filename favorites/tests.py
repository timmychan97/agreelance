from http import HTTPStatus

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, Client
from django.urls import reverse
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium import webdriver

import favorites
from favorites.models import Favorite
from projects.models import ProjectCategory, Project, Task, TaskOffer


class FavoriteButtonTests(TestCase):

    def setUp(self):
        User.objects.create_user(username='testUser', password='12345')
        project_owner = User.objects.create_user(username='testProjectOwner', password='12345')
        category = ProjectCategory.objects.create(name="testProjectCategory")
        self.project = Project.objects.create(user=project_owner.profile, title="testProject",
                                              description="testDescription", category=category, status="o")
        self.client = Client()

    # Server and Database
    def test_add_favorite(self):
        self.client.login(username='testUser', password='12345')
        url = reverse(favorites.views.new_favorite, args=[self.project.id])

        response = self.client.post(url)
        self.assertTrue(response.status_code == HTTPStatus.OK)
        self.assertTrue(Favorite.objects.filter(project=self.project).exists())

        expect_response = b"New favorite stored in database"
        self.assertEqual(expect_response, response.content)

    def test_cascade_on_delete_project(self):
        user = User.objects.get(username='testUser')
        favorite = Favorite.objects.create(user=user.profile, project=self.project)
        self.assertEqual(favorite, Favorite.objects.get(user=user.profile))

        self.project.delete()
        with self.assertRaises(Favorite.DoesNotExist):
            Favorite.objects.get(user=user.profile)

    def test_cascade_on_delete_user(self):
        user = User.objects.get(username='testUser')
        favorite = Favorite.objects.create(user=user.profile, project=self.project)
        self.assertEqual(favorite, Favorite.objects.get(user=user.profile))

        user.delete()
        with self.assertRaises(Favorite.DoesNotExist):
            Favorite.objects.get(project=self.project)

    def test_add_favorite_of_non_existing_project(self):
        self.client.login(username='testUser', password='12345')
        url = reverse(favorites.views.new_favorite, args=[41])

        response = self.client.post(url)
        self.assertTrue(response.status_code == HTTPStatus.NOT_FOUND)

    def test_not_authenticated_activities(self):
        url = reverse(favorites.views.new_favorite, args=[self.project.id])
        response = self.client.post(url)
        self.assertRedirects(response, "/user/login/?next=/favorites/new/1")

        url = reverse(favorites.views.delete_favorite, args=[self.project.id])
        response = self.client.post(url)
        self.assertRedirects(response, "/user/login/?next=/favorites/delete/1")


class FavoriteButtonLiveTests(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Start Chrome
        options = Options()
        if settings.IS_CI:
            # Must have for gitlab
            options.add_argument('headless')

            options.add_argument('window-size=1200x600')
            options.add_argument('disable-gpu')
            options.add_argument("no-sandbox")
        else:
            # Show the test on non CI environment
            options.add_argument("start-maximized")

        # Optional argument, if not specified will search path.
        cls.driver = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        # ConnectionResetError: [WinError 10054] will show due to a django, selenium bug.
        cls.driver.quit()
        cls.driver = None
        super(FavoriteButtonLiveTests, cls).tearDownClass()

    def setUp(self):
        # Fill DB with objects
        user = User.objects.create_user(username='testUser', password='12345')
        project_owner = User.objects.create_user(username='testProjectOwner', password='12345')

        category = ProjectCategory.objects.create(name="testProjectCategory")
        project = Project.objects.create(user=project_owner.profile, title="testProject",
                                         description="testDescription", category=category, status="o")
        task = Task.objects.create(project=project, title="testTask", description="testDescription")
        TaskOffer.objects.create(task=task, title='testTaskOffer', offerer=user.profile,
                                 feedback="testUserFeedback")

    def tearDown(self):
        # No need to logout since django do a reset on the browser for each test
        pass

    def test_button_favorite_toggle(self):
        self.driver.get(self.live_server_url)
        self.driver.implicitly_wait(3)
        self.login()

        # Navigate to Favorites tab
        self.driver.find_element_by_xpath('//*[@id="navbarSupportedContent"]//a[text()="Favorites"]').click()

        # Assert no favorites yet
        project_list = self.driver.find_element_by_css_selector(".row.projects-view")
        self.assertEqual(len(project_list.get_property("children")), 0)

        # Navigate to Projects tab
        self.driver.find_element_by_xpath('//*[@id="navbarSupportedContent"]//a[text()="Projects"]').click()

        # Click on first project
        self.driver.find_element_by_css_selector("#nav-tabContent .row.projects-view").get_property("children")[
            0].click()

        # Click Add to Favorites
        self.driver.find_element_by_css_selector(".favorite-btn.btn.btn-outline-primary").click()

        # Navigate to Favorites tab
        self.driver.find_element_by_xpath('//*[@id="navbarSupportedContent"]//a[text()="Favorites"]').click()

        # Assert there is one project
        project_list = self.driver.find_element_by_css_selector(".row.projects-view")
        self.assertEqual(len(project_list.get_property("children")), 1)

        # Click on a favorite project
        self.driver.find_element_by_css_selector(".row.projects-view").get_property("children")[0].click()

        # Click Remove from Favorites
        self.driver.find_element_by_css_selector(".favorite-btn.btn.btn-outline-primary").click()

        # Navigate to Favorites tab
        self.driver.find_element_by_xpath('//*[@id="navbarSupportedContent"]//a[text()="Favorites"]').click()

        # Assert no favorites
        project_list = self.driver.find_element_by_css_selector(".row.projects-view")
        self.assertEqual(len(project_list.get_property("children")), 0)

    def test_button_is_not_shown_to_unauthorized_users(self):
        self.driver.get(self.live_server_url)

        # Spend max 3 seconds to wait for an element to be found
        self.driver.implicitly_wait(3)

        # Navigate to Projects tab
        self.driver.find_element_by_xpath('//*[@id="navbarSupportedContent"]//a[text()="Projects"]').click()

        # Click on first project
        self.driver.find_element_by_css_selector("#nav-tabContent .row.projects-view").get_property("children")[
            0].click()

        # Assert no "Add to Favorites" button
        with self.assertRaises(NoSuchElementException):
            self.driver.find_element_by_css_selector(".favorite-btn.btn.btn-outline-primary")

    def login(self):
        print("- LOGIN --------")
        sign_in_button = self.driver.find_element_by_xpath('//*[@id="navbarSupportedContent"]//a[text()="Sign in"]')
        sign_in_button.click()

        self.driver.find_element_by_id("id_username").send_keys("testUser")
        password_field = self.driver.find_element_by_id("id_password")
        password_field.send_keys("12345")
        password_field.send_keys(Keys.RETURN)

    def logout(self):
        print("- LOGOUT --------")
        sign_in_button = self.driver.find_element_by_xpath('//*[@id="navbarSupportedContent"]//a[text()="Sign out"]')
        sign_in_button.click()
