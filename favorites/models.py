from django.db import models
from user.models import Profile
from projects.models import Project


class Favorite(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.user.username + " " + self.project.title
