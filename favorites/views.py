from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from projects.models import Project
from favorites.models import Favorite


# Create your views here.
@login_required
def favorites(request):
    user_favorites = Favorite.objects.filter(user=request.user.profile).prefetch_related("project")
    projects = []
    for f in user_favorites:
        projects += [f.project]
    return render(request, 'favorites/favorites.html', {'projects': projects})


@login_required
def new_favorite(request, project_id):
    f = Favorite()
    f.user = request.user.profile
    f.project = get_object_or_404(Project, pk=project_id)
    f.save()
    return HttpResponse("New favorite stored in database")


@login_required
def delete_favorite(request, project_id):
    favorite = Favorite.objects.filter(project__id=project_id, user=request.user.profile)
    favorite.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
