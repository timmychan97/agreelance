from django.urls import path
from . import views


urlpatterns = [
    path('', views.favorites, name='favorites'),
    path('new/<project_id>', views.new_favorite, name='new_favorite'),
    path('delete/<project_id>', views.delete_favorite, name='delete_favorite'),
]
