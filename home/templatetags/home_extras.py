from django import template
from projects.models import Task, TaskOffer

register = template.Library()


@register.filter
def check_nr_pending_offers(project):
    task_offer_statuses = TaskOfferStatuses()
    for task in project.tasks.all():
        task_offers = task.taskoffer_set.all()
        for task_offer in task_offers:
            task_offer_statuses.add_to_count(task_offer.status)

    return task_offer_statuses.get_dict()['pending']


@register.filter
def check_nr_user_offers(project, user):
    task_offer_statuses = TaskOfferStatuses()
    for task in project.tasks.all():
        task_offers = task.taskoffer_set.filter(offerer=user.profile)
        for task_offer in task_offers:
            task_offer_statuses.add_to_count(task_offer.status)

    return task_offer_statuses.get_dict()


@register.filter
def task_status(task):
    if task.status == Task.PENDING_ACCEPTANCE:
        return "You have deliveries waiting for acceptance"
    elif task.status == Task.PENDING_PAYMENT:
        return "You have deliveries waiting for payment"
    elif task.status == Task.PAYMENT_SENT:
        return "You have sent payment"
    return "You are awaiting delivery"


@register.filter
def get_task_statuses(project):
    task_statuses = TaskStatuses()
    for task in project.tasks.all():
        task_statuses.add_to_count(task.status)
    return task_statuses.get_dict()


@register.filter
def all_tasks(project):
    return project.tasks.all()


@register.filter
def offers(task):
    task_offers = task.taskoffer_set.all()
    print("home-extras, Task offers:", task_offers)
    msg = "No offers"
    if len(task_offers) > 0:
        x = 0
        msg = "You have "
        for t in task_offers:
            x += 1
            if t.status == TaskOffer.ACCEPTED:
                return "You have accepted an offer for this task"
        msg += str(x) + " pending offers"
    return msg


@register.filter
def get_user_task_statuses(project, user):
    task_statuses = TaskStatuses()
    for task in project.tasks.all():
        try:
            task_offer = task.taskoffer_set.get(status=TaskOffer.ACCEPTED)
            if task_offer.offerer == user.profile:
                task_statuses.add_to_count(task.status)

        except TaskOffer.DoesNotExist:
            pass
    return task_statuses.get_dict()


class TaskStatuses:
    """
    Keep track of the count of each task's status.
    """
    def __init__(self):
        self.awaiting_delivery = 0
        self.pending_acceptance = 0
        self.pending_payment = 0
        self.payment_sent = 0
        self.declined_delivery = 0

    def get_dict(self):
        return {
            'awaiting_delivery': self.awaiting_delivery,
            'pending_acceptance': self.pending_acceptance,
            'pending_payment': self.pending_payment,
            'payment_sent': self.payment_sent,
            'declined_delivery': self.declined_delivery,
        }

    def add_to_count(self, status):
        if status == Task.AWAITING_DELIVERY:
            self.awaiting_delivery += 1
        elif status == Task.PENDING_ACCEPTANCE:
            self.pending_acceptance += 1
        elif status == Task.PENDING_PAYMENT:
            self.pending_payment += 1
        elif status == Task.PAYMENT_SENT:
            self.payment_sent += 1
        elif status == Task.DECLINED_DELIVERY:
            self.declined_delivery += 1


class TaskOfferStatuses:
    """
    Keep track of the count of each task_offer's status.
    """
    def __init__(self):
        self.pending = 0
        self.declined = 0
        self.accepted = 0

    def get_dict(self):
        return {
            'pending': self.pending,
            'declined': self.declined,
            'accepted': self.accepted,
        }

    def add_to_count(self, status):
        if status == TaskOffer.PENDING:
            self.pending += 1
        elif status == TaskOffer.ACCEPTED:
            self.accepted += 1
        elif status == TaskOffer.DECLINED:
            self.declined += 1
