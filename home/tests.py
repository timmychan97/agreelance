from django.contrib.auth.models import User
from django.test import TestCase

from home.templatetags.home_extras import check_nr_pending_offers, check_nr_user_offers, get_task_statuses, \
    get_user_task_statuses
from projects.models import Project, Task, ProjectCategory, TaskOffer


class HomeExtras(TestCase):
    fixtures = ['test_seed_standard.json']

    def setUp(self):

        self.project_owner = User.objects.get(username="admin")

        category = ProjectCategory.objects.create(name="testProjectCategory")
        self.project = Project.objects.create(user=self.project_owner.profile, title="testProject",
                                              description="testDescription", category=category, status="o")

        self.offerer1 = User.objects.get(username="joe")
        self.offerer2 = User.objects.get(username="random_user")

        # Create a count of tasks with status as listed in task_statuses
        task_statuses = {
            'awaiting_delivery': 4,
            'pending_acceptance': 1,
            'pending_payment': 4,
            'payment_sent': 3,
            'declined_delivery': 7,
        }
        # Create total of 19 tasks with 5 different statuses
        self.createTasks(task_statuses)

        # For each task, create a count a task offers with status as listed in task_offer_statuses
        user1_task_offer_statuses = {
            'pending': 3,
            'declined': 4,
            'accepted': 1,
        }
        user2_task_offer_statuses = {
            'pending': 2,
            'declined': 5,
            'accepted': 0,
        }
        # Create total of 8 task offers for each of 19 tasks.
        for task in self.project.tasks.all():
            self.createTaskOffers(self.offerer1, task, user1_task_offer_statuses)
            self.createTaskOffers(self.offerer2, task, user2_task_offer_statuses)

        """
        Total number of task offers are:
         - 95 pending offers
         - 171 declined offers
         - 19 accepted offers
        """

    def createTasks(self, task_statuses):
        for status, number in task_statuses.items():
            if status == 'awaiting_delivery':
                self.createNTask(number, Task.AWAITING_DELIVERY)
            if status == 'pending_acceptance':
                self.createNTask(number, Task.PENDING_ACCEPTANCE)
            if status == 'pending_payment':
                self.createNTask(number, Task.PENDING_PAYMENT)
            if status == 'payment_sent':
                self.createNTask(number, Task.PAYMENT_SENT)
            if status == 'declined_delivery':
                self.createNTask(number, Task.DECLINED_DELIVERY)

    def createTaskOffers(self, user, task, task_offer_statuses):
        for status, number in task_offer_statuses.items():
            if status == 'pending':
                self.createNTaskOffer(user, number, task, TaskOffer.PENDING)
            if status == 'declined':
                self.createNTaskOffer(user, number, task, TaskOffer.DECLINED)
            if status == 'accepted':
                self.createNTaskOffer(user, number, task, TaskOffer.ACCEPTED)

    def createNTask(self, n, status):
        for i in range(n):
            Task.objects.create(project=self.project, title=".t", description="d", budget=123,
                                status=status)

    def createNTaskOffer(self, user, n, task, status):
        for i in range(n):
            TaskOffer.objects.create(task=task, title=".t pending", description="d", price=123,
                                     offerer=user.profile, status=status)

    def test_check_nr_pending_offers(self):
        self.assertEqual(check_nr_pending_offers(self.project), 95)

    def test_check_nr_user_offers(self):
        expected1 = {
            'pending': 3*19,
            'declined': 4*19,
            'accepted': 1*19,
        }
        self.assertEqual(check_nr_user_offers(self.project, self.offerer1), expected1)

        expected2 = {
            'pending': 2*19,
            'declined': 5*19,
            'accepted': 0*19,
        }
        self.assertEqual(check_nr_user_offers(self.project, self.offerer2), expected2)

    def test_get_task_statuses(self):
        expected = {
            'awaiting_delivery': 4,
            'pending_acceptance': 1,
            'pending_payment': 4,
            'payment_sent': 3,
            'declined_delivery': 7,
        }
        self.assertEqual(get_task_statuses(self.project), expected)

    def test_get_user_task_statuses(self):
        expected = {
            'awaiting_delivery': 4,
            'pending_acceptance': 1,
            'pending_payment': 4,
            'payment_sent': 3,
            'declined_delivery': 7,
        }
        self.assertEqual(get_user_task_statuses(self.project, self.offerer1), expected)

        expected = {
            'awaiting_delivery': 0,
            'pending_acceptance': 0,
            'pending_payment': 0,
            'payment_sent': 0,
            'declined_delivery': 0,
        }
        self.assertEqual(get_user_task_statuses(self.project, self.offerer2), expected)
